.. hysop_install_guide:

HySoP quick start guide
========================

HySoP is a library dedicated to high performance direct numerical simulation of fluid related problems based on semi-lagrangian particle methods, for hybrid architectures providing multiple compute devices including CPUs, GPUs or MICs.
HySoP is written in Python (main user interface and high level functionnalities) and Fortran.

`From Docker images (recommended) <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop#from-docker-images-recommended>`_
----------------------------------------------------------------------------------------------------------------------------------------

`Quick Start <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop#quick-start>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`How to improve your HySoP experience <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop#how-to-improve-your-hysop-experience>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`From sources <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop#from-sources>`_
--------------------------------------------------------------------------------------------------

`Installation <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop#installation>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Configuration options <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop#configuration-options>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Testing <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop#testing>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Dependencies <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop#dependencies>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
