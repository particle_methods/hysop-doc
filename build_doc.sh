#!/usr/bin/env bash
set -eux -o pipefail

: ${CI_PROJECT_DIR:?"Please set environment variable CI_PROJECT_DIR with 'hysop-doc' repository (absolute) path."}

BUILD_DIR="${BUILD_DIR:=$CI_PROJECT_DIR/build-doc}" 
HYSOP_SRC_DIR="${HYSOP_DIR:=/tmp/hysop-src}" # Path to hysop sources (git clone below)

# Get and install  hysop

git clone --depth 1 https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop.git ${HYSOP_SRC_DIR}
meson setup build ${HYSOP_SRC_DIR} 
meson compile -C build &> /dev/null
meson install -C build &> /dev/null
rm -rf build

# Build doc

meson setup build-doc ${CI_PROJECT_DIR}
meson compile -C build-doc 

exit 0
