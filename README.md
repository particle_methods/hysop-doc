# HySoP-doc

[![Licence](https://img.shields.io/badge/licence-APLv2-green.svg)](https://www.apache.org/licenses/LICENSE-2.0)
[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop-doc/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop-doc/-/commits/main)


This project is used to handle Hysop software documentation creation

- web page
- user manual
- API / software references


Generated (gitlab-ci process) doc is available here: [https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc](https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc).
