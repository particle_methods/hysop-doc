.. hysop documentation master file, created by
   sphinx-quickstart on Thu Nov  4 13:58:20 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HySoP's webpage!
===========================
.. toctree::
   :hidden:
   :maxdepth: 3
   :caption: Contents:

   install_guide/index
   getting_started/index
   users_guide/index
   examples/index
   reference/index
   devel/index
   biblio
   todos
   credits
   license




.. image:: figures/logo_hysop_rgb.png

HySoP (Hybrid Simulation with Particles) is a library mainly dedicated to high performance numerical simulation of fluid flows problems based on semi-lagrangian particle methods, for hybrid and distributed architectures providing multiple compute devices including CPUs and GPUs.

Source code is available at: https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop

Gallery
-------
Passive scalar advection

.. image:: figures/advection_scalaire_passif.png

Wake of a sphere

.. image:: figures/sphere_3D_wake.png

Sediment flows

.. image:: figures/sediments.png

Dissolution of carbonate rock

.. image:: figures/dissolution_castlegate.png

Playlist of several simulations

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLYzyXvTeUmgFaXod0jCjPLao-AgfsGBhZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Programming elements
--------------------

The high level functionalities and the user interface are mainly written in Python using the object oriented programming model.

This choice was made because of the large software integration benefits it can provide. Moreover, the object oriented programming model offers a flexible framework to implement scientific libraries when compared to the imperative programming model. It is also a good choice for the users as the Python language is easy to use for beginners while experienced programmers can pick it up very quickly. The numerical solvers are mostly implemented using compiled languages such as Fortran, C/C++, or OpenCL for obvious performance reasons. It is also possible to implement numerical algorithms using directly Python, which is an interpreted language, hence slower for critical code paths under heavy arithmetic or memory load. The Python language support is however the key for rapid development cycles of experimental features. It also allows to easily implement routines that compute simulation statistics during runtime, relieving most of the user post-processing efforts and enabling live simulation monitoring.

The compiled Fortran and C++ compute backends allow us to integrate a variety of external dependencies by connecting them to the main HySoP python module with interface wrappers such as F2PY or SWIG. Note that many scientific libraries already provide Python interfaces so that they can be directly used in python without needing the user to implement his own wrapper. In addition to the compiled languages, the library offers the possibility to compile generated code just-in-time during execution. This is the case for OpenCL, the language used to drive OpenCL-compatible accelerators, like GPUs, but also to translate python methods to fast machine code by using the Numba just-in-time compiler.

References
----------

HySoP has been used to produce numerical results in the following references:

.. bibliography:: hysop.bib
   :list: bullet
   :style: unsrt
   :all:
