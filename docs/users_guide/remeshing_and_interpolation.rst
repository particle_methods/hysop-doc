.. _remesh_and_interpolate:


Interpolation and remeshing methods
------------------------------------------

Interpolation
^^^^^^^^^^^^^^^^^^

Use :class:`~hysop.numerics.interpolation.polynomial.PolynomialInterpolator` to interpolate a field (scalar or vector)
from one mesh to another.

Interpolator is defined with respect to interpolation
degree. Pre-defined interpolators are provided in
:obj:`~hysop.numerics.interpolation.polynomial.PolynomialInterpolation`

>>> from hysop.numerics.interpolation.polynomial import PolynomialInterpolator
>>> from hysop.numerics.interpolation.polynomial import PolynomialInterpolation
>>> # bi-linear interpolator
>>> bl = PolynomialInterpolator.build_interpolator(PolynomialInterpolation.LINEAR, dim=2)

**What**: interpolation of a field, defined as a numpy array from one
"grid" to another (supposed to be nested).

>>> import numpy as np
>>> bli = bl.generate_subgrid_interpolator(grid_ratio=(2,4), dtype=np.float64)
>>> F = [[0,  1],
...      [10, 11]]
>>> bli(np.asarray(F))
array([[ 0.  ,  0.25,  0.5 ,  0.75,  1.  ],
       [ 5.  ,  5.25,  5.5 ,  5.75,  6.  ],
       [10.  , 10.25, 10.5 , 10.75, 11.  ]])


Devel notes
^^^^^^^^^^^^^^^^^

topo_source :

* used to compute/get the 'left points' coordinates
* ghost points layer size must fit with interpolation scheme
* used to compute/get space step size
* used to compute/get domain origin


topo_target :

* used to compute work arrays shape
* must be included into topo_source domain
* used to compute/get list of indices of interpolated points

We must take into account the three cases below:

* interpolate from one grid to another, grid defined as topology. Easy case, we know both topo/
* interpolate from one grid to 'particles', when particles are initialized everywhere on a grid.
  We also know both topo so, it works.
* interpolate from one grid to 'particles' when a threshold is used to initialize particles.
  In that case, the set of indices of particles must be provided to the interpolation caller since
  topo_target is not known (not defined indeed). This to set the variable ic in __call__.
  THIS REMAINS TO BE DONE/TEST PROPERLY
