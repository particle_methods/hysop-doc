.. _space:


Domains, data distribution and space discretisation
===================================================


Roughly speaking, building a problem in HySoP consists in describing some fields (vectors and/or scalars), defined on physical domains, then some operators are applied to compute the behavior of those fields in time and space. This part of the manual describes fields, domains and the way data are discretized and distributed in space.

.. toctree::
   :maxdepth: 2

   domains
