.. _differential_operations:

.. currentmodule:: hysop.numerics.differential_operations

Vector calculous based on finite differences
--------------------------------------------

.. currentmodule:: hysop.fields

.. todo ::
    Describe how to define a :class:`~hysop.operator.custom_symbolic.CustomSymbolicOperator` and how use symbols associated to an existing :py:obj:`~hysop.fields.continuous_field.Field` using vector calculus operators :py:meth:`~continuous_field.FieldContainerI.gradient`, :py:meth:`~continuous_field.FieldContainerI.laplacian`, :py:meth:`~continuous_field.FieldContainerI.div`, :py:meth:`~continuous_field.FieldContainerI.curl`. See :mod:`hysop.symbolic` module for more details.
