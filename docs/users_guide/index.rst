.. _user_guide:


############################
HySoP User Guide
############################


HySoP (Hybrid SimulatiOn with Particles) is a library dedicated to
flow simulation based on particles methods,
on hybrid (multi CPU-GPU) architecture.

It is written in Python (high level functionnalities) and Fortran.

This document presents the main concepts and functionnalities of the software.


From the user point of view, the main usecase  will decompose into the three main steps as in follwing use case diagram:


.. figure:: /figures/user_hysop.*
   :class: with-border

   Use case diagram.


1. Problem description: as mathematical PDE formalism using domain, variables and operators;
2. Problem initialisation: after describing the numerical methods with their parameters, the user may specify the main cartesian grid resolution, the mesh decomposition for parallel simulations, and the compute backend. The ordering of the different operators is enforced by the `HySoP` user interface. Finally the user must describe how to initialise the variables of the problem. To summarize, from the library point of view, at the end of this step, all memory allocations (user an internal use) are performed and  all the computations and communication layout is known.
3. Problem solving: after defining  a few more parameters for time dependant problems (i.e. time steps), the computations can start applying the operators in order.

Following the same color code as that of the use case figure, the simplified diagram of `HySop` is given below, illustrating the interaction of the decoupled entities "Domain, Discretizations, Variables, Operators, Numerics and Tools, Problem".

.. figure:: /figures/diagram_hysop_simplified.*
   :class: with-border

   Simplified `HySoP` package diagram and most significant classes (the yellow "a" dots correspond to the main classes attributes and the pink "m" dots to the main classes methods.


..
   The whole classes and functions documentation is available in auto-generated
   Doxygen pages of `Numerics <http://siconos.gforge.inria.fr/Numerics/>`_
   and `Kernel <http://siconos.gforge.inria.fr/Kernel>`_ for C++ or in
   `Front-End pages <http://siconos.gforge.inria.fr/FrontEnd/html/>`_ for
   the python interface.


.. toctree::
   :maxdepth: 3

   equations
   space
   fields
   operators
   problems
   topologies
   numerics
   tools
