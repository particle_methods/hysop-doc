 .. _numerics:

Numerical operations
====================

This module handles all the 'low-level' operations mostly applied on numpy arrays.

.. toctree::
   :maxdepth: 2

   finite_differences
   differential_operations
   remeshing_and_interpolation
   odesolvers
