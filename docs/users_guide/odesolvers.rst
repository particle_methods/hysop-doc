.. _odesolvers:

ODE solvers
-----------------

.. currentmodule hysop.numerics.odesolvers

Consider the following problem:

.. math::

   \frac{d x(t)}{dt} &=& rhs(t, x(t)) \\
   x(t_0) &=& x_0


with x and rhs some vector fields defined on a 1,2 or 3D domain.

The following time integrators are implemented in hysop:

* forward Euler,  :obj:`~hysop.numerics.odesolvers.runge_kutta.Euler`,
* forward Runge-Kutta of order 2, 3 and 4 : :obj:`~hysop.numerics.odesolvers.runge_kutta.RK2`, :obj:`~hysop.numerics.odesolvers.runge_kutta.RK3` and :obj:`~hysop.numerics.odesolvers.runge_kutta.RK4`

See :mod:`~hysop.numerics.odesolvers.runge_kutta` for details, and
:class:`~hysop.numerics.odesolvers.runge_kutta.ExplicitRungeKutta` to build any Runge-Kutta scheme.


Usage
^^^^^^^^^^


>>> import numpy as np
>>> from hysop.numerics.odesolvers.runge_kutta import Euler
>>> def f(out, X, t, step, steps):
...     for k in X.keys():
...         out[k][...] = X[k]
...
>>> y = np.linspace(0,1,11)
>>> v = {'y': y,}
>>> Euler(v, f, 0.1)
{'y': array([0.  , 0.11, 0.22, 0.33, 0.44, 0.55, 0.66, 0.77, 0.88, 0.99, 1.1 ])}

To build a time integrator, it is necessary to define a python function to compute the right-hand side.
This function must be of the following form::

  def some_func(out, X, t, step, steps, **kwds):
      out[name][...] = expr(X[name])


* `out` and `X` are dictionaries with variables names as keys values
  are np.arrays contiaining datas
* `name` is the name of the variable
* t is the time
* `X` must not be modified by the function
* `step` equals the current step in RK algorithms with `steps` stages
* Extra paramters can be forwarded throw extra keywords arguments
