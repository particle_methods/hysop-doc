.. _about_doc:

How to document classes in HySoP
=================================

We try to fit as much as possible with `numpydoc guide <https://numpydoc.readthedocs.io/en/latest/>`_
guide standards.

See the files fields/continuous.py and operator/drag_and_lift.py which
are used as reference files for documentation.

Mind the need of `raw strings litterals <https://docs.python.org/3/reference/lexical_analysis.html#string-and-bytes-literals>`_ in python docstrings for keeping the `\stuff` from math
environments `".. :math:"`.

Mind the 'space' character before colon character `":"` in parameters or
attributes definitions.

Any code sample in docstring should be written as a doctest code
sample (see `https://docs.python.org/3/library/doctest.html`).
