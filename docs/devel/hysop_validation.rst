.. _hysop_validation:


HySoP validation
================

`HySoP` code is tested against a set of unitary and integration tests as
well as several examples provided. These tests are run in a continuous
integration process attached to the Gitlab instance hosting the
code. `Continuous integration <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop/-/pipelines>`_ is running in docker containers on
several resources hosted by French National Centre for Scientific
Research and author's university. Several docker images are considered
as reproducing main users configurations either with GPU or CPU OpenCL
platforms.

Docker images used for continuous integration are finally completed by
an installation of `HySoP` package. These images are freely available as
ready-to-use for users `HySoP registry <https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop/container_registry>`_. Beside this all inclusive way of getting the
software, another process is to install all dependencies together with
the `HySoP` package itself from sources. The install process rely on
meson build system.

These documentation pages are build automatically against the last
version in sources 'master' branch.
Triggered pipeline on hysop or hysop-doc use the same CI image to pull hysop
source code and doc sources for generating static documentation (user
guide, developpers guide, ...) and API generated documentation. The
doctests are runned on documentation pages building.

Few commits message filters are activated in hysop repository:

- No CI when commit message starts with `wip`;
- Skip testing pipeline (env, config, build, install and test) when
  commit message contains `[skip-testing]` (trigger only online doc
  building);
- Force re-building the CI docker image first when
  commit message contains `[docker-build]`.
