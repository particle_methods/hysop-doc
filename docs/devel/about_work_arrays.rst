.. _about_work_arrays:

Temporary work arays in HySoP
=============================

Internal temporary buffers may be required by computational operators (buffers for Runge-Kutta scheme for example). By defaul
t, each operator handles its own work arrays and manage their memory.

But, to improve performance and limit the required memory amount, user can provide some external
storage for those arrays, for example to share them between several operators.

Each operator has a :py:meth:`~Computational.get_work_properties` function, returning information on the required memory for
internal stuff. On graph building, all operators' memory requests are
handled to be merged.



See :class:`~hysop.core.memory.memory_request.MemoryRequest`
See :meth:`~hysop.core.graph.computational_graph.ComputationalGraph.get_work_properties`
