.. _devel:


############################
HySoP Developers Guide
############################

This part is for developers of HySoP and provides guidelines on the "proper" ways to write python code, documentation, examples, tests ... in HySoP.

.. toctree::
   :maxdepth: 2

   about_doc
   coding_guidelines
   compiled_code_interface
   about_operators_graph
   about_work_arrays
   about_codegen
   memo_sphinx
   hysop_validation
