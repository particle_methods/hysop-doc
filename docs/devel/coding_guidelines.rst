.. _coding_guidelines:

How to write proper code in HySoP
=================================

Developper should follow python recommandations for code style.

Lower-level functions should be tested in the appropriate unitary
tests folders.

Higher-level functionalities should be exposed in an example file.
