.. _memo_sphinx:

Reminder for sphinx utilities
=============================

Source code for this page (see 'View source page' on page header)

To print source Code (mind the double ':' before start code block)::

  def foo(i):
      print(2*i)

  for i in range(3):
      foo(i)

But, it is more robust to turn sample source code into doctest:

>>> def foo(i):
...     print(2*i)
...
>>> for i in range(3):
...     foo(i)
...
0
2
4

Hyperlinks `HySoP Web`_.


.. math::
   \begin{eqnarray}
   a &=& \frac{1}{2}\\
   \alpha &=& 3 \times \nabla \omega
   \end{eqnarray}

To print a link to some HySoP object:

.. currentmodule:: hysop.operator.discrete

An example of domain used for defining :class:`~hysop.fields.continuous_field.Field` and operators : :class:`~hysop.domain.box.Box`.

Reference to :ref:`domains`.

References
------------

In documentation, we recommend to use footnotecite in pages. The full
bibliography is dedicated to references using HySoP\
:footcite:p:`etancelin:2014`. Do not forget the Sphinx entry `.. footbibliography::` before end of file.



Links
-----

 * rst :
   http://sphinx-doc.org/rest.html

 * math support
   http://sphinx-doc.org/ext/math.html

 * numpydoc :
   https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt


.. _Hysop Web: https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc


.. footbibliography::
