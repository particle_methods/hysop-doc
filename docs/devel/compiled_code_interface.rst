.. _fortran2python:

Calling external code C/C++/Fortran
===========================================

This page propose a tutorial to call some fortran or C/C++ subroutines
in python within hysop.

C/C++ code
----------------------

.. todo::
      Write Cython example (in developper doc).


Fortran code
-------------------

Sources:

    * some fortran files which contain some subroutines inside some modules
    * a pyf file


.. todo::
      Write f2py example (in developper doc).





Kind values
^^^^^^^^^^^^^^

According to f2py doc: "Currently, F2PY can handle only <type spec>(kind=<kindselector>) declarations where <kindselector> is a numeric integer (e.g. 1, 2, 4,...) but not a function call KIND(..) or any other expression. F2PY needs to know what would be the corresponding C type and a general solution for that would be too complicated to implement."

To overcome this, a .f2py_f2cmap is generated during the build, from hysop/f2py_f2cmap.in.
It contains all the required mappings defined as a dictionnary, like::

  {'integer':{'c_int':'int'}, 'real':{'real64':'double', 'wp':'double'}}

In that example, a fortran :code:'real(kind=wp)' will be mapped into a double-precision real in python.
