.. _bilbio:

Bibliography
#################

HySoP has been used to produce numerical results in the following references:

.. bibliography:: hysop.bib
   :list: bullet
   :style: unsrt
   :all:
