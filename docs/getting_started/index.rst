.. _getting_started:

Getting Started With HySoP
==========================

Basics : running a simulation with HySoP
----------------------------------------

The problem to be solved must be described using Python language either interactively or in a python script.
If you are not at ease with this language, we strongly encourage you to check one of the numerous python tutorials available on the web. Try for example https://docs.python.org/3/tutorial/. A basic understanding of numpy may also be useful : http://www.numpy.org.
Numpy is the python package for scientific computing on which HySoP relies, especially for arrays handling.


Some, python references:

* https://www.python.org/about/gettingstarted/, for beginners with python
* https://docs.python.org/3/, python official documentation, including tutorials
* http://www.numpy.org, numpy, scipy documentation
* http://hplgit.github.io/homepage/books.html

Interactive session
*******************

For an interactive use, we recommend ipython or even ipython notebook. Start the interpreter and then import hysop and/or its
submodules. For example, try in a terminal window::

  ipython

and

  >>> from hysop import Box
  >>> from hysop.operators import Poisson
  >>> dom = Box(dim=2)  # 2D [0:1]^2 domain

Use script
**********

Interactive session is very useful for tests, basic understanding of hysop functionnalities but real simulation must
be executed by using a script.

To do so, write a python script 'myscript.py' which contains all python commands required to describe and solve your problem (see next part or user guide) and run in a terminal window::

  python myscript.py

for sequential run, or::

  mpirun -np 4 python myscript.py

for parallel run on 4 mpi processes.

The best way to begin with HySoP is to try one of the numerous examples available in the distribution.


Basics : describe a problem in HySoP
------------------------------------

This short introduction presents the basic objects to know to describe and solve properly a problem with HySoP.

HySoP provides a framework for flow simulation, based on particles methods.


From the user point of view, the main usecase  will decompose into the three main steps as in follwing use case diagram:


.. figure:: /figures/user_hysop.*
   :class: with-border

   Use case diagram.

1. Problem description: as mathematical PDE formalism using domain, variables and operators;
2. Problem initialisation: after describing the numerical methods with their parameters, the user may specify the main cartesian grid resolution, the mesh decomposition for parallel simulations, and the compute backend. The ordering of the different operators is enforced by the `HySoP` user interface. Finally the user must describe how to initialise the variables of the problem. To summarize, from the library point of view, at the end of this step, all memory allocations (user an internal use) are performed and  all the computations and communication layout is known.
3. Problem solving: after defining  a few more parameters for time dependant problems (i.e. time steps), the computations can start applying the operators in order.


Here is a short glossary of the objects which form a simulation process within HySoP:

**Domain**: a physical domain, defined by its origin, its size and some types of boundary conditions. Different space discretisations and data distributions can be associated with a domain. See details in :ref:`domains`.

**Fields**: the unknowns of your problem, the 'classical' mathematical vector or scalar fields, i.e. a function which associates a scalar or a vector to each point of the space.
Fields obviously depends on time and space. See details in :ref:`fields`.

**Operator**: a set of equations (most of the time ode), defined on a domain, with some fields as unknowns. See details in :ref:`operators`.

**Problem**: a sequence of operators associated with a simulation.

All of the objects defined above are high-level continuous objects, only descriptions indeed.
In particular, none of them depends on the type of execution (parallel or sequential) and on the number of mpi processes involved.
Indeed, to proceed with the simulation, it is necessary to define space and time discretization, to choose some numerical methods to solve each operator, in some words to configure and discretize the whole process. Thus, we need to introduce the following objects:

**Discretization** : space discretization of the global domain (grid resolution, space step size ...).

**Topology** : description of how data are distributed among mpi processes and of the space discretisation (global and local to each process)

For details about discretisation and data distribution, check :ref:`topologies`.

**Simulation**: description of the time discretisation (start and end time, time step ...)

Consider for example the following advection problem,

.. math::

   \frac{\partial \rho(\boldsymbol{x},t)}{\partial t} + v(\boldsymbol{x},t).\nabla\rho(\boldsymbol{x},t) = 0



where :math:`\rho`, a scalar field, and :math:`v`, a vector field, are the unknowns, defined on a box-shaped domain with some given values on the boundaries
and initial values at time :math:`t=t_0`.

Then, in HySoP, you will have to define two *Fields*, representing  :math:`\rho` and :math:`v`, a 3d domain, an operator 'advection' and a problem including only one operator. Such a process may look like::


  >>> from hysop import Box, Field, Simulation, Problem
  >>> from hysop.defaults import  TimeParameters
  >>> from hysop.operators import Advection
  >>>
  >>> # A box-shaped 3d domain
  >>> dom = Box(length=[1., 1., 1.], origin=[0., 0., 0.])
  >>> # Define some continuous fields
  >>> v = Field(domain=dom, name='velocity', is_vector=True)
  >>> rho = Field(domain=dom, name='rho')
  >>> # Space discretisation (cartesian)
  >>> n = (64, 64, 64)
  >>> # Time discretization
  >>> t, dt = TimeParameters()
  >>> simu = Simulation(t=t, dt=dt, dt0=0.01)
  >>> # Define a continuous operator, that will be discretized in space and time
  >>> adv = Advection(v, rho, variables={v:n, rho:n}, dt=dt)
  >>> # Define a problem containing the operator
  >>> problem = Problem()
  >>> problem.insert(adv)
  >>>
  >>> # Discretize everything, and prepare lower level functionnalities
  >>> problem.build()
  >>>
  >>> # ... some stuff to initialize fields values ...
  >>>
  >>> # solve advection for the current time
  >>> problem.solve(simu)
  >>>
  >>> # ... post process ...
  >>>
  >>> problem.finalize()

Notice that you don't need a deep understanding about 'parallel' things, mpi or gpu, to use HySoP, at least for a basic usage.
As you can see in the example above, we try to hide as much as possible those parallel stuff from the high-level user interface. Just keep in mind that your programm may run on several processes and that data may be distributed accross different memory areas.
