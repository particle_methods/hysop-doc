---
title: An hybrid computing implementation for remeshed particle method with operator splitting
author: HySoP team
date: 4th of September 2024 -- jacaXVII
documentclass: beamer
classoption : 11pt
listings: true
lang: en
section-titles: false
bibliography: ../hysop.bib
institute:
 - Universite de Pau et des Pays de l’Adour, E2S UPPA, CNRS, LMAP, Pau
 - Laboratory M2N, CNAM, Paris
 - Laboratoire Jean Kuntzmann, Grenoble INP, Université Grenoble Alpes and CNRS, Grenoble
header-includes:
- |
  \usepackage{bbm}
  \usepackage{tikz}
  \usetikzlibrary{snakes,arrows,shapes}
  \def\div{\mathsf{div}}
  \def\w{\omega}
  \def\eps{\varepsilon}
  \def\em1{\varepsilon^{-1}}
  \def\ds{\displaystyle}
  \def\p{\partial}
  \def\1{\mathbb{1}\!}
  \def\dpart#1#2{\frac{\p #1}{\p #2}}
  \def\DDownarrow{\textsf{\scalebox{1}[2]{$\Downarrow$}}}
  \renewcommand*{\bibfont}{\tiny}
  \graphicspath{{img/}}
...

\frametitle{Contents}
\tableofcontents

# HySoP

### Purposes

Initial goals:

- Scope: numerical simulation of fluid flows
  * DNS
  * Obstacles \pause
- Means: hybrid numerical methods
  * Operator splitting
  * Remeshed particle methods \pause
- Resources: High performance computing
  * CPU+GPU (hybrid computing)
  * distributed architectures
  * range from laptop to clusters \pause
- Audience:  Research in applied maths
  * Quick prototyping
  * Enhanced performances

### Software

- Open-Source:
  [https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop)
- 120k lines of code (83% Python, 15% Fortran)
- Distribution:
  * from sources (gitlab)
  * from docker images (pre-installed)
  * binder for on-the-fly demos

### People

- Jean-Matthieu Etancelin\footnote{LMAP, UPPA, Pau}
- Chloé Mimeau\footnote{M2N, CNAM, Paris}
- Jean-Baptiste Keck\footnote{LJK, UGA, Grenoble}
- Franck Perignon\footnotemark[2]
- Nicolas Grima\footnotemark[1]
- Christophe Picard\footnotemark[2]
- Georges-Henri Cottet\footnotemark[2]



### Main features

Application : Scalar reactive transport in presence of turbulent
incompressible fluid
flow
with obstacles in 3-periodic domain


$$
\left\{\begin{aligned}
\frac{\partial\boldsymbol{\omega}}{\partial t}& + (\mathbf{u} \cdot \nabla) \boldsymbol{\omega} - (\boldsymbol{\omega} \cdot \nabla) \mathbf{u} - \dfrac{1}{Re} \Delta \boldsymbol{\omega} = \nabla \times \mathbf{f}_{ext}\\
\frac{\partial\theta}{\partial t}& + (\mathbf{u} \cdot \nabla) \theta -
\kappa \Delta \theta  = r(\theta)\\
\Delta \mathbf{u} &= - \nabla \times \boldsymbol{\omega}
\end{aligned}\right.
$$


- $\mathbf{u}$: velocity
- $\boldsymbol{\omega}$: vorticity ($\boldsymbol{\omega}:=\nabla\times  \mathbf{u}$)
- $\theta$: transported quantities
- $\mathbf{f}_{ext}$: External forces
- $Re$: Reynolds number
- $\kappa$: diffusivity of $\theta$
- $r$: reaction term





### Main mathematical features


- Remeshed vortex method
- Operator splitting:
  * Viscous splitting
  * Dimensional splitting
- High order remeshing formulas
- Brinkman penalization for solid immersed bodies

### Main mathematical features


- Remeshed vortex method

Particles : $\boldsymbol x_p$ position, $\boldsymbol u_p$ velocity, $\boldsymbol \omega_p$ transported vorticity

$$
\frac{\partial\boldsymbol{\omega}}{\partial t} + (\mathbf{u} \cdot \nabla) \boldsymbol{\omega} = \boldsymbol F
\rightarrow
\left\{\begin{aligned}
\frac{d}{dt} \boldsymbol x_p &= \boldsymbol u_p\\
\frac{d}{dt} \boldsymbol \omega_p &= \boldsymbol v_p \boldsymbol F_p
\end{aligned}\right.
$$

\pause

With remeshing on a cartesian grid at each timestep

$$\boldsymbol \omega_i = \sum_p \boldsymbol \omega_p W(\boldsymbol x_p, \boldsymbol x_i)$$


$W$ is a interpolation kernel (remeshing formula)

### Main mathematical features

\small

- Operator splitting:

Viscous splitting
$$
\left\{\begin{aligned}
\frac{d}{dt} \boldsymbol x_p &= \boldsymbol u_p\\
\frac{d}{dt} \boldsymbol \omega_p &= \boldsymbol v_p \boldsymbol F_p
\end{aligned}\right.
\rightarrow
(A) \left\{\begin{aligned}
\frac{d}{dt} \boldsymbol x_p &= \boldsymbol u_p\\
\frac{d}{dt} \boldsymbol \omega_p &= 0
\end{aligned}\right. ,\qquad
(V)\left\{\begin{aligned}
\frac{d}{dt} \boldsymbol x_p &= 0\\
\frac{d}{dt} \boldsymbol \omega_p &= \boldsymbol v_p \boldsymbol F_p
\end{aligned}\right.
$$\pause

Dimensional splitting
$$
(A) \left\{\begin{aligned}
\frac{d}{dt} \boldsymbol x_p &= \boldsymbol u_p\\
\frac{d}{dt} \boldsymbol \omega_p &= 0
\end{aligned}\right.
\rightarrow
(A^x) \left\{\begin{aligned}
\frac{d}{dt} \boldsymbol x_p^x &= \boldsymbol u_p^x\\
\frac{d}{dt} \boldsymbol x_p^{yz} &= \boldsymbol 0\\
\frac{d}{dt} \boldsymbol \omega_p &= 0
\end{aligned}\right. ,\qquad
(A^y) \left\{\begin{aligned}
\frac{d}{dt} \boldsymbol x_p^y &= \boldsymbol u_p^y\\
\frac{d}{dt} \boldsymbol x_p^{xz} &= \boldsymbol 0\\
\frac{d}{dt} \boldsymbol \omega_p &= 0
\end{aligned}\right.
$$\pause

Then compose operator $V\circ A$ or $A^x\circ A^y\circ A^z$

At discrete level: use best suited numerical method

### Main mathematical features

<!-- - High order remeshing formulas -->

<!-- Dimensionnal splitting of advection $\longrightarrow$ 1D stencils for -->
<!-- remesing -->

<!-- Order is depending on $W$ kernel properties (moments conservation and regularity) -->

<!-- Use of piecewise polynomials satisfying discrete properties.\pause -->

- Brinkman penalization for solid immersed bodies

$$\mathbf{f}_{ext} = \lambda_\chi (\boldsymbol u_s - \boldsymbol u)$$

$\boldsymbol u_s$ : solid body velocity

$\lambda_\chi$ : penalization factor (as inverse of material
permeabilty)



### Main computer science features


- User interface close to DSL
  * Domain
  * Field / parameters
  * Operators
  * Problem
  * $+$ numerical dicretization and numerical method choices
- Abstraction of low level details (data layout, IO, execution
  architecture)
- Code generation
  * from symbolic expressions
  * from elementwize operations
- Easy prototyping:
  * Customizable operator either from symbolic expressions or code
    sample provided by user
- Accelerated code (OpenCL) is tuned at runtime (micro-benchmarks)


# Success stories
### Applications

\small

|  Applications                                        |   Involved equations                                            |  Reference        |
|:-----------------------------------------------------|:----------------------------------------------------------------|:------------------|
| - Bluff body flows                                   | Navier-Stokes                                                   | [@mimeau:2016; @mimeau:2021b]    |
| - Transport of passive scalar at high Schmidt number | Navier-Stokes and a passive scalar advection-diffusion          | [@cottet:2014]    |
| - Sedimentation in high Schmidt number flows         | Navier-Stokes coupled with scalars advection-diffusion          | [@keck:2019; @keck:2021]      |
| - Passive flow control using porous media            | Brinkman-Navier-Stokes                                          | [@mimeau:2017]    |
| - Porous media reactive transport at pore-scale      | Darcy-Brinkman-Stokes coupled with advection-diffusion-reaction | [@etancelin:2020] |



### Multi-GPU performances

\href{run:/home/jmetancelin/Recherche/Productions/2017-0911_seminaire_LMAP/SC64_Froggy.mp4}{Passive scalar transport (2015)} [@cottet:2014]

![](img/passive_scalar_transport.png)


### Multi-GPU performances

\href{run:/home/jmetancelin/Recherche/Productions/2017-0911_seminaire_LMAP/SC64_Froggy.mp4}{Passive
scalar transport (2015)} [@cottet:2014]

\includegraphics[page=2,height=.7\textheight]{img/passive_scalar_transport_weak_scalability_sc64.pdf}

- $Sc = 16$: config = 128^3 | 512^3 on 4CPU+1GPU
- $Sc = 64$: config = 128^3 | 1024^3 on 4CPU+1GPU

### OpenCL portable performances

\small

Reactive transport at porescale (2024) [@etancelin:2020]: Dissolution
of carbonate rock sample

![](img/CarbonateDissolution.png)

### OpenCL portable performances

\small


Reactive transport at porescale (2024): Precipitation
of carbonate rock sample

![](img/CarbonatePrecipitation_perfsOpenCL.pdf)

# High Performance Computing
### Automatic data layout management

Depending on numerical methods and backend library:

- MPI topologies (slices or pencils)
- ghosts layers (stencil boundary for domain decomposition)
- transpositions of datas (Fortran or C backend lib)
- data locality (host or device)

Almost all hidden to user




### Micro-benchmarks: device tuning of performances

OpenCL code generation

- Natural way of using OpenCL code (natively uses JIT)
- Can offer wide range of capabilites\pause

Implementation:

- String formatting from Python context managers
- Code generation from Sympy codegen features
    * more flexibility to user
    * but more engineering effort in hysop

- With autotuning
    * wide space of parameters to explore (several strategies
      implemented)
    * Cache results in files for subsequent executions


### Micro-benchmarks: device tuning of performances

- OpenCL code generation + autotuning [@keck:2019]

![](img/CodeGenerationAutotuning.png){ width=95% }



### Micro-benchmarks: device tuning of performances

Example  [@keck:2019]

![](img/advection_rooflne.png){ width=95% }

# Future developments and perspectives
### Future

Software improvements:

- need a MPI+OpenCL efficient FFT library
    * use FFTW for traditionnal CPU implementation
    * use clFFT for accelerator but sequential

- need to investigate inter GPU mpi communications\pause

Maths/applications features

- Reactive transport in porous media (done in private way)
- LES turbulence model (ongoing work at CNAM Paris)
- Fluid-structure interaction (see this afternoon, future work in Pau)\pause


Community:

- Start discussions for benchmarking ?

# References
