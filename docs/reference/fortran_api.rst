.. _fortran_api:

Fortran, C/C++ API
==================

Documentation for functions and classes written in other languages than python is generated thanks
to doxygen.

This includes fftw and scales' interfaces.

See `Fortran API <../doxygen/html/index.html>`_.
