.. _hysop_reference:

HySoP reference (auto generated) documentation
==============================================

Generated according to version |version|

.. toctree::
   :maxdepth: 2
   :glob:
	      

   ../apidoc/*
   fortran_api
